
public class SumOfDigitsInANumber {

	public static void main(String[] args) {
		int isDigitSumStatus = isDigitSum(543, -3);
		System.out.println( "Is digit sum status: "+ isDigitSumStatus);

	}

	private static int isDigitSum(int n, int sum) {
		
		if( n < 0 || sum < 0 ) {
			return -1;
		}
		
		
		int nSum = 0;
		// n = Math.abs(n);
		while( n%10 != 0 ) {
			int mod = n%10;
			n = n/10;
			nSum = nSum+mod;
		}
		
		//System.out.println( "nSum: "+nSum );
		if( nSum < sum )
			return 1;
		
		return 0;
	}

}
