
public class KSmallFactor {

	public static void main(String[] args) {
		int k = 10;
		int n = 22;
		boolean hasSmallFactor = hasSmallFactor(k, n);
		System.out.println("Has small factor: " + hasSmallFactor);
	}

	private static boolean hasSmallFactor(int k, int n) {
		int factorCount = 0;
		for (int i = 2; i < k; i++) {
			if (n % i == 0) {
				factorCount = factorCount + 1;
			}
		}

		int[] factors = new int[factorCount];

		for (int i = 2; i < k; i++ ) {
			int value = i;
			if (n % value == 0) {
				factorCount = factorCount - 1;
				factors[factorCount] = value;
			}
		}

		
		for (int i = 0; i < factors.length; i++) {
			int value = factors[i];
			for (int j = 0; j < factors.length; j++) {
				if (i == j) {
					continue;
				}

				int otherValue = factors[j];
				int product = value * otherValue;
				if (product == n) {
					return true;
				}
			}
		}

		return false;
	}

}
