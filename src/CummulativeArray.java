
public class CummulativeArray {

	public static void main(String[] args) {

		int[] a = new int[] {-3, -3, 6, 12, 24};
		int isLayered = isCummulative(a);
		System.out.println(isLayered);

	}

	private static int isCummulative(int[] a) {
		if (a == null || a.length < 2 )
			return 0;
		
		int nSum = 0;
		for( int i = 0; i < a.length-1; i++ ) {
			int n = i+1;
			int nValue = a[ n ];
			
			
			//for( int j = 0; j < n; j++ ) {
				int value = a[i];
				nSum = nSum + value;
			//}
			
			if( nSum != nValue )
				return 0;
		}

		
		return 1;
	}

}
