
public class DifferenceOfSumOfOddAndEvenNumbersInArray {
	
	public static void main( String[] args ) {
		int[] numbers = {};
		int diff = getDiffOfSumOfOddAndEvenNumbers( numbers );
		System.out.print( diff );
		
	}

	private static int getDiffOfSumOfOddAndEvenNumbers(int[] numbers) {
		if( numbers == null || numbers.length == 0 )
			return 0;
		int x = 0;
		int y = 0;
		
		for( int number: numbers ) {
			if( number%2 == 0 )
				y=y+number;
			else
				x = x+number;
		}
		
		return x-y;
	}

}
