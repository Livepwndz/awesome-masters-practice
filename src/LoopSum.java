
public class LoopSum {

	public static void main(String[] args) {

		int[] a = new int[] {1, 4, 5, 6};
		int n = 4;
		int sum = loopSum(a, n);
		System.out.println(sum);

	}

	private static int loopSum(int[] a, int n) {
		if (a == null || a.length == 0)
			return 0;

		if (n < 1)
			return 0;
		
		
		int sum = 0;
		int j = 0;
		for (int i = 0; j < n;) {
			int value = a[i];
			sum = sum + value;

			if (i == a.length - 1 && i < n ) {
				i = 0;
			} else {
				i = i + 1;
			}
			
			j = j + 1;
		}

		return sum;
	}

}
