
public class LayeredArray {

	public static void main(String[] args) {

		int[] a = new int[] {};
		int isLayered = isLayered(a);
		System.out.println(isLayered);

	}

	private static int isLayered(int[] a) {
		if (a == null || a.length < 2)
			return 0;

		for (int i = 0; i < a.length - 1;) {
			int value1 = a[i];
			i = i + 1;
			int value2 = a[i];

			if (value1 != value2) {
				return 0;
			}
			

			// Find next bigger value
			for (int j = i + 1; j < a.length;) {
				int nextValue = a[j];

				if (nextValue < value2) {
					return 0;
				}

				if (nextValue == value2) {
					j = j + 1;
					continue;
				}

				if (nextValue > value2) {
					i = j;
					break;
				}

			}
		}

		return 1;
	}

}
