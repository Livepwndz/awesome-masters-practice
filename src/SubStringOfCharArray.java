import java.util.Arrays;

public class SubStringOfCharArray {

	public static void main(String[] args) {
		int[] arrayOne = {};
		int[] arrayTwo = {};

		int[] arrayOfCommonValues = getArrayOfCommonValues(arrayOne, arrayTwo);
		Arrays.stream(arrayOfCommonValues).forEach(System.out::println);

	}

	private static int[] getArrayOfCommonValues(int[] arrayOne, int[] arrayTwo) {
		if (arrayOne == null || arrayOne.length == 0)
			return new int[] {};

		if (arrayTwo == null || arrayTwo.length == 0)
			return new int[] {};

		int[] longerArray = arrayOne;
		int[] shorterArray = arrayTwo;

		if (arrayTwo.length > arrayOne.length) {
			longerArray = arrayTwo;
			shorterArray = arrayOne;
		}

		int commonValueCount = 0;
		for (int i = 0; i < longerArray.length; i++) {
			int longerArrayValue = longerArray[i];
			for (int x = 0; x < shorterArray.length; x++) {
				int shorterArrayValue = shorterArray[x];
				if (shorterArrayValue == longerArrayValue) {
					commonValueCount++;
				}
			}
		}

		int[] commonValuesArray = new int[commonValueCount];

		int commonValueIndex = 0;
		for (int i = 0; i < longerArray.length; i++) {
			int longerArrayValue = longerArray[i];
			for (int x = 0; x < shorterArray.length; x++) {
				int shorterArrayValue = shorterArray[x];
				if (shorterArrayValue == longerArrayValue) {
					commonValuesArray[commonValueIndex] = shorterArrayValue;
					commonValueIndex++;
				}
			}
		}

		return commonValuesArray;
	}

}
