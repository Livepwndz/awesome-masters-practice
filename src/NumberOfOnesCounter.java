

public class NumberOfOnesCounter {

	public static void main(String[] args) {
		int number = 15;
		int count = countOnes(number);
		System.out.println("Count of ones: " + count);

	}

	private static int countOnes(int number) {
		int count = 0;
		if( number == 0 ) {
			return count;
		}
			
			
		count = 1;
		while( number > 1 ) {
			int mod = number%2;
			if( mod == 1 ) {
				count++;
			}
			
			number = number/2;
		}
		
		return count;
	}



}
