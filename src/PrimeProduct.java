
public class PrimeProduct {

	public static void main(String[] args) {
		int isPrimeProductStatus = isPrimeProduct(22);
		System.out.println("Is Prime Product status: " + isPrimeProductStatus);

	}

	private static int isPrimeProduct(int n) {
		// boolean isPrime = Util.isPrime( n );
		/*
		 * if( !isPrime ) { return 0; }
		 */

		int primeCount = 0;
		for (int i = 2; i < n; i++) {
			int value = i;
			if (Util.isPrime(value)) {
				primeCount++;
			}
		}

		if (primeCount <= 1) {
			return 0;
		}

		int[] primes = new int[primeCount];
		for (int i = 2; i < n; i++) {
			int value = i;
			if (Util.isPrime(value)) {
				int primeIndex = primeCount - 1;
				primeCount = primeIndex;
				primes[primeIndex] = value;
				if (primeIndex == 0) {
					break;
				}
			}
		}

		for (int i = 0; i < primes.length; i++) {
			int prime = primes[i];
			for (int j = 0; j < primes.length; j++) {
				if (i == j) {
					continue;
				}

				int otherPrime = primes[j];
				int primeProduct = prime * otherPrime;
				if (primeProduct == n) {
					return 1;
				}
			}
		}

		return 0;
	}

}
