import java.util.Arrays;
import java.util.stream.Collectors;

public class POE {

	public static void main(String[] args) {
		int[] arrayOne = {1, 2, 10, 3, 4};

		int poe = getArrayPoe(arrayOne);
		System.out.println("Poe: " + poe);

	}

	private static int getArrayPoe(int[] arrayOne) {

		int n = arrayOne.length;
		for (int i = 0; i < n; i++) {
			int idx = i;
			int firstSum = 0;
			for (int j = 0; j < idx; j++) {
				firstSum = firstSum + arrayOne[j];
			}

			int secondSum = 0;
			for (int k = idx + 1; k < n; k++) {
				secondSum = secondSum + arrayOne[k];
			}

			if (firstSum == secondSum) {
				return idx;
			}
		}

		return -1;
	}

}
