
public class NormalNumber {
	
	public static void main( String[] args ) {
		int isNormalStatus = isNormal( 9 );
		System.out.println( "Normal status: "+isNormalStatus );
	}

	private static int isNormal(int n) {
		for( int i = 2; i < n; i++ ) {
			int mod = n%i;
			if(mod == 0 && i%2 != 0) {
				return 0;
			}
		}
		return 1;
	}

}
