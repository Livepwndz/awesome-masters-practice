
public class CompleteArray {
	
	public static void main( String[] args) { 
		int isCompleteStatus = isComplete( new int[] {2,2,4,3});
		System.out.println( "Is complete: "+isCompleteStatus );
		
	}

	private static int isComplete(int[] a) {
		if( a == null || a.length <= 1 ) {
			return 0;
		}
		
		int min = 0;
		int max = 0;
		boolean isEvenPresent = false;
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			
			boolean isEven = value%2 == 0;
			
			if( isEven ){
				
				if( !isEvenPresent ) {
					isEvenPresent = true;
					min = value;
					max = value;
				}
				
				if( value < min ) {
					min = value;
				} 
				
				if( value > max) {
					max = value;
				}
			}
			
			
		}
		
		System.out.println( "Min: "+min );
		System.out.println( "Max: "+max );
		
		if( !isEvenPresent ) {
			return 0;
		}
		
		if( min == max ) {
			return 0;
		}
		
		for( int i = min+1; i < max; i++) {
			int valueInRange = i;
			boolean isValueInRangePresentInArray = false;
			for( int j = 0; j < a.length; j++ ) {
				int value = a[j];
				if( value == valueInRange ) {
					isValueInRangePresentInArray = true;
					break;
				}
				
			}
			
			if( !isValueInRangePresentInArray) {
				return 0;
			}
			
		}
		return 1;
	}

}
