
public class MagicArray {

	public static void main(String[] args) {
		int isMagicStatus = isMagic( new int[] {8, 5, -5, 5, 3});
		System.out.println( "Is magic status: "+isMagicStatus );

	}

	private static int isMagic(int[] a) {
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		int sumOfPrimes = 0;
		
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( isPrime( value )) {
				sumOfPrimes = sumOfPrimes + value;
			}
		}
		
		int valueOfFirstElement = a[0];
		if( valueOfFirstElement == sumOfPrimes ) {
			return 1;
		}
		
		return 0;
	}

	private static boolean isPrime(int value) {
		if( value < 0 ) {
			return false;
		}
		for (int x = 2; x < value; x++) {
			int mod = value % x;
			if (mod == 0) {
				return false;
			}
		}
		return true;
	}

}
