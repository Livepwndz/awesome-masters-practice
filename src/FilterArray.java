
public class FilterArray {

	public static void main(String[] args) {
		int isFilterStatus = isFilter( new int[] {7} );
		System.out.println( "Is filter status: "+isFilterStatus );

	}

	private static int isFilter(int[] a) {
		if( a == null || a.length == 0 ) {
			return 0;
		}
		
		boolean is13Present = false;
		boolean is11Present = false;
		boolean is7Present = false;
		boolean is9Present = false;
		for( int i = 0; i < a.length; i++ ) {
			int value = a[i];
			if( value == 11 ) {
				is11Present = true;
			} else if( value == 13 ) {
				is13Present = true;
			} else if( value == 7 ) {
				is7Present = true;
			} else if( value == 9 ) {
				is9Present = true;
			}
		}
		
		if( ( is9Present && !is11Present)  ) {
			return 0;
		}
		
		if( ( is7Present && is13Present)  ) {
			return 0;
		}
		
		
		return 1;
	}

}
