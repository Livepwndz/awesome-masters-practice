
public class OddValentArray {
	
	public static void main( String[] args ) {
		int oddValentStatus = isOddValent( new int[] {2, 2, 2, 4, 4});
		System.out.println( "Is odd valent: "+oddValentStatus );
	}

	private static int isOddValent(int[] a) {
		if( a == null || a.length < 2 ) {
			return 0;
		}
		
		boolean isOdd = false;
		boolean isValueMultiple = false;
		for( int i = 0; i < a.length; i++) {
			int value = a[i];
			if( !isOdd  ) {
				isOdd = value%2 != 0;
			}
			
			for( int j = 0; j < a.length && !isValueMultiple; j++ ) {
				if( j == i )
					continue;
				
				int otherValue = a[j];
				if( otherValue == value ) {
					isValueMultiple = true;
				}
					
				
			}
			
			if( isOdd && isValueMultiple ) {
				return 1;
			}
		}
		
		
		return 0;
	}

}
